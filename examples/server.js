var udt4 = require('../udt4');
var svr = udt4.createServer(function (data) {
      console.log("Server Callback : " + data);
})

svr.listen(1337, '127.0.0.1');

//setTimeout(function(){svr.close()}, 2000)

svr.on('data', function(data) {
	console.log("Explicit On : " + data)
})

svr.on('connection', function(data) {
	console.log("connection On : " + data)
})

svr.on('error', function(data) {
	console.log("Error : " + data)
})

svr.on('disconnect', function(data) {
	console.log("disconnect : " + data)
})

console.log('UDT4 Server running at 127.0.0.1:1337');
