var udt4 = require('../udt4');
var test = require('tap').test
var spawn = require('child_process').spawn;

test('test server start', function (t) {

   t.plan(2); 

   var svr = udt4.createServer(function (data) {
       console.log("hello from create server")
   });
   
   t.type(svr, 'object', 'Server is an object');
  
   server  = spawn('node', ['examples/server', '1337']);

   server.stdout.on('data', function(data) {
      console.log(data.toString())
      if(data.toString().indexOf('Explicit On : data') > -1) {
        t.ok(data.toString().indexOf('Explicit On : data') > -1, 'Data called');
        server.kill('SIGHUP')
      }
   });
   setTimeout(function() {

   var clt = udt4.createClient();
    clt.connect(1337);
    clt.write('data');
    clt.end();

   },100)

});
