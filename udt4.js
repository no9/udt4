var udt4 = require('bindings')('udt4')
var util = require("util");
var events = require("events");
var Stream = require('stream').Stream;

var Client = udt4.Client;
var Server = udt4.Server;

inherits(Client, events.EventEmitter);
inherits(Server, events.EventEmitter);

var svr = {};
function UDT4 () {
         this.udt4 = udt4;
}

//var stream = fs.createReadStream

UDT4.createReadStream = function(port, address) {
   var stream = new Stream();
   stream.readable = true;
   try {
      svr = udt4.Server();
      svr.listen(port, address);
   }catch(Error){
      throw Error;
   }

   svr.on('data', function(data){
    stream.emit('data', data);
   });

   svr.on('error', function(data) {
      console.log('error called' + data)
      stream.emit('error', data);
    })

   svr.on('connection', function(data) {
      stream.emit('connection', data)
    })

    svr.on('disconnect', function(data) {
      console.log('disconnect called')
      stream.emit('disconnect', data)
    })


   stream.end = function (data) {
      if(data)
         console.log("end called")
         stream.write(data);
         stream.emit('end');
      }

   stream.destroy = function () {
      console.log("destroy called")
      stream.emit('close');
   }
  return stream; 
}

UDT4.createServer = function(callback) {
   if (arguments.length < 1)
      throw new Error('createServer() requires a `callback` argument');

   if (typeof callback != 'function')
      throw new Error('createServer() requires a callback function argument');
   
   
   svr = udt4.Server();
   svr.on('data', callback);
   
   return svr
}

function inherits(target, source) {
       for (var k in source.prototype) {
          target.prototype[k] = source.prototype[k];
       }
}

UDT4.createClient = function() {
   if (arguments.length > 1)
      throw new Error('createClient() only takes optional an configuration list. Please see documentation');
   return udt4.Client();
}

module.exports = UDT4 
