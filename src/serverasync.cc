#include <nan.h>
#include <uv.h>
#include <iostream>
#include <sstream>
#include <udt.h>

#include "cc.h"
#include "serverasync.h"

using namespace v8;
using namespace std;

namespace udt4 {

struct ThreadData {
         UDTSOCKET *socket;
};

uv_loop_t *loop;
uv_async_t data_async;
uv_async_t connection_async;
uv_async_t disconnect_async;
uv_async_t error_async;
uv_async_t close_async;

uv_thread_t recvdata_id; //Should be an collection


static bool isListening = true;
static Persistent<Object> persistentEmitter;
static void recvdata(void*);  

      ServerAsync::ServerAsync(Local<Object> eventemitter, NanCallback *callback, int port)
         : NanAsyncWorker(callback), port(port) {

            NanAssignPersistent(persistentEmitter, eventemitter);
            loop = uv_default_loop();
            uv_async_init(loop, &data_async, OnDataEvent);
            uv_async_init(loop, &connection_async, OnConnectionEvent);
            uv_async_init(loop, &disconnect_async, OnDisconnectEvent);
            uv_async_init(loop, &error_async, OnErrorEvent);
              
         }

      ServerAsync::~ServerAsync() {
        NanDisposePersistent(persistentEmitter);

      }

      void ServerAsync::OnDataEvent(uv_async_t *handle, int status) {
      
      char* data = ((char*) handle->data);

      NanEscapableScope();
      
      Handle<Value> argv[2] = {
            NanNew("data"), // event name
            NanNew(data)
       };
 
      Local<Object> emitter = NanNew(persistentEmitter);
      NanMakeCallback(emitter, "emit", 2, argv);
      cout << "EVENTED : " << data << endl;
     }

     void ServerAsync::OnErrorEvent(uv_async_t *handle, int status) {
      
      char* data = ((char*) handle->data);

      NanEscapableScope();
      
      Handle<Value> argv[2] = {
            NanNew("error"), // event name
            NanNew(data)
       };
 
      Local<Object> emitter = NanNew(persistentEmitter);
      NanMakeCallback(emitter, "emit", 2, argv);
      
     }


     void ServerAsync::OnConnectionEvent(uv_async_t *handle, int status) {
        char* data = ((char*) handle->data);

      NanEscapableScope();
      
      Handle<Value> argv[2] = {
            NanNew("connection"), // event name
            NanNew(data)
       };
 
      Local<Object> emitter = NanNew(persistentEmitter);
      NanMakeCallback(emitter, "emit", 2, argv);
      
     }

    void ServerAsync::OnDisconnectEvent(uv_async_t *handle, int status) {
      NanEscapableScope();

      char* data = ((char*) handle->data);
      
      Handle<Value> argv[2] = {
            NanNew("connection"), // event name
            NanNew(data)
       };
 
      Local<Object> emitter = NanNew(persistentEmitter);
      NanMakeCallback(emitter, "emit", 2, argv);
      
     }

     void ServerAsync::OnCloseEvent(uv_async_t *handle, int status) {
         cout << "WARNING - Method not impmlemented.\n" << endl;
         /*
         uv_thread_join(&recvdata_id);
         UDTSOCKET cl_serv = *(UDTSOCKET*)handle ->data;
         //TODO: This causes a sementation fault. 
         //UDT::close(cl_serv);
         //UDT::cleanup();
         */
         Handle<Value> argv[1] = {
            NanNew("close"), // event name
            
         };
         Local<Object> emitter = NanNew(persistentEmitter);
         NanMakeCallback(emitter, "emit", 1, argv);
         
     }

     void ServerAsync::Close() {
  
         isListening = false;
         uv_async_send(&close_async);       
  
     } 
      // Executed inside the worker-thread.
      // // It is not safe to access V8, or V8 data structures
      // // here, so everything we need for input and output
      // // should go on `this`.
      void ServerAsync::Execute () {
         
         UDT::startup();

         addrinfo hints;
         addrinfo* res;
         
         memset(&hints, 0, sizeof(struct addrinfo));

         hints.ai_flags = AI_PASSIVE;
         hints.ai_family = AF_INET;
         hints.ai_socktype = SOCK_STREAM;
         
         std::stringstream out;
         out << port;
         string service(out.str());

         if (0 != getaddrinfo(NULL, service.c_str(), &hints, &res)){
            //TODO: This needs converting to a message
            error_async.data = (void *) UDT::getlasterror().getErrorMessage();
            uv_async_send(&error_async);
            
               cout << "illegal port number or port is busy.\n" << endl;
         }
         

         UDTSOCKET serv;
         

         serv = UDT::socket(res->ai_family, res->ai_socktype, res->ai_protocol);
         //close_async.data = (void *) serv;
         uv_loop_t *ex_loop;
         ex_loop = uv_default_loop();
         uv_async_init(ex_loop, &close_async, OnCloseEvent);   

         if (UDT::ERROR == UDT::bind(serv, res->ai_addr, res->ai_addrlen))
         {
            error_async.data = (void *) UDT::getlasterror().getErrorMessage();
            uv_async_send(&error_async);
            
            //cout << "bind: " << UDT::getlasterror().getErrorMessage() << endl;
            return;
         }

         freeaddrinfo(res);
         //TODO: Should this be an on server started event?
         //cout << "server is ready at port: " << service << endl;

         if (UDT::ERROR == UDT::listen(serv, 10))
         {
            error_async.data = (void *) UDT::getlasterror().getErrorMessage();
            uv_async_send(&error_async);
                   
            //cout << "listen: " << UDT::getlasterror().getErrorMessage() << endl;
            return;
         }

         sockaddr_storage clientaddr;
         int addrlen = sizeof(clientaddr);

         UDTSOCKET recver;
         while (isListening)
         {
            if (UDT::INVALID_SOCK == (recver = UDT::accept(serv, (sockaddr*)&clientaddr, &addrlen)))
            {
               error_async.data = (void *) UDT::getlasterror().getErrorMessage();
               uv_async_send(&error_async);
               //cout << "accept: " << UDT::getlasterror().getErrorMessage() << endl;
               return ;
            }
            char clienthost[NI_MAXHOST];
            char clientservice[NI_MAXSERV];
            getnameinfo((sockaddr *)&clientaddr, addrlen, clienthost, sizeof(clienthost), clientservice, sizeof(clientservice), NI_NUMERICHOST|NI_NUMERICSERV);
            
            connection_async.data = clientservice;
            uv_async_send(&connection_async);

            //cout << "new connection: " << clienthost << ":" << clientservice << endl;

            struct ThreadData targs;
                           targs.socket = new UDTSOCKET(recver);
            
            uv_thread_create(&recvdata_id, recvdata, &targs);
       
         }
         
         cout << "Closing serv: " << endl; 
         //UDT::close(serv);
      }
      
      // // Executed when the async work is complete
      // // this function will be run inside the main event loop
      // // so it is safe to use V8 again
      void ServerAsync::HandleOKCallback () {
         NanScope();
         //callback->Call(0, NULL);
      }
      
      static void recvdata(void* ustruct) 
      {
         
         struct ThreadData *lstruct = (struct ThreadData*) ustruct;
         UDTSOCKET recver = *(UDTSOCKET*)lstruct ->socket;
         delete (UDTSOCKET*)lstruct->socket;

         char* data;
         int size = 100000;
         data = new char[size];
         char* retval;
         retval = new char[size];

         while (true)
         {
           int rsize = 0;
           int rs;
            while (rsize < size)
            {
                int rcv_size;
                int var_size = sizeof(int);
                UDT::getsockopt(recver, 0, UDT_RCVDATA, &rcv_size, &var_size);
                if (UDT::ERROR == (rs = UDT::recv(recver, data + rsize, size - rsize, 0)))
                {
                   error_async.data = (void *) UDT::getlasterror().getErrorMessage();
                   uv_async_send(&error_async);
                   
                   cout << "recv:" << UDT::getlasterror().getErrorMessage() << endl;
                   break;
                }
                rsize += rs;
            }

            memcpy( retval, data, size);

            data_async.data = retval;
            
            
            //cout << "recv:" << data << endl;
            
            uv_async_send(&data_async);
            
            if (rsize < size){
               break;
            }
         }
            delete [] data;

            UDT::close(recver);
         }
}