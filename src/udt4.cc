#include <node.h>
#include <nan.h>

#include "client.h"
#include "server.h"

#pragma comment (lib, "ws2_32.lib")

using namespace v8;
using namespace std;

namespace udt4 {

   void Init(Handle<Object> exports) {
      
      Client::Init(exports);
      Server::Init(exports);

      NanScope();
      
   }

   NODE_MODULE(udt4, Init);
}
